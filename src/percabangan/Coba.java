
package percabangan;

import java.util.Scanner;

public class Coba {

    public static void main(String[] args) {
        String identitas = "Dimas Angkasa / X RPL 6 / 19";
        System.out.println("Identitas : " + identitas);
        
        Scanner masukan = new Scanner(System.in);
        System.out.print("Nilai a ? ");
        int a = masukan.nextInt();
        
        System.out.println("\nIF.. 1");
        if(a < 5) System.out.println("Nilai a kurang dari 5");
        
        System.out.println("\nIF.. 2");
        if(a == 5) System.out.println("Nilai a sama dengan 5");
        
        System.out.println("\nIF.. 3");
        if(a > 5) {
            System.out.println("Isi variabel a : " + a);
            System.out.println("nilai a lebih dari 5");
        }
        
        System.out.println("------------------------------");
        
        System.out.println("\nIf..ELSE..");
        if(a < 5 ){
            System.out.println("Isi variabel a : " + a);
            System.out.println("Nilai a kurang dari 5");
        }else System.out.println("nilai a lebih dari atau sama denga 5");
        
        System.out.println("------------------------------");
        
        System.out.println("\nIF..ELSE IF..ELSE.. 1");
        if(a < 5){
            System.out.println("nilai a kurang dari 5");
        }else if(a == 5){
            System.out.println("nilai a sama dengan 5");
        }else if(a > 5)
            System.out.println("nilai a lebih dari 5");
        
        System.out.println("------------------------------");
        
        System.out.println("\nIF..ELSE IF..ELSE.. 2");
        if(a < 2){
            System.out.println("nilai a kurang dari 2");
        }else if(a < 4){
            System.out.println("nilai a kurang dari 4");
        }else if(a < 6){
            System.out.println("nilai a kurang dari 6");
        }else if(a == 6){
            System.out.println("nilai a sama dengan 6");
        }else {
            System.out.println("nilai a lebih dari 6");
        }
        
        System.out.println("------------------------------");
        
        System.out.println("\nNested if");
        if(a < 7){
            System.out.println("nilai a kurang dari 7");
            if(a > 2)
                System.out.println("nilai a lebih dari 2");
            if(a < 4)
                System.out.println("nilai a kurang dari 4");
        }
        
        System.out.println("------------------------------");
        
        System.out.println("\nSWITCH..CASE");
        switch(a){
            case 1: System.out.println("nilai a => 1"); break;
            case 2: System.out.println("nilai a => 2"); break;
            case 3:
            case 4: System.out.println("nilai a => 3 atau 4"); break;
            case 5: System.out.println("nilai a => 5"); break;
            default: System.out.println("nilai a bukan antara 1-5");
        }
        
        System.out.println("------------------------------");
        
        int nilai;
        System.out.print("\nNilai Anda ? ");
        nilai = masukan.nextInt();
        
        String grade = (nilai < 75) ? "Belum Kompeten" : "Kompeten";
        System.out.println(nilai + " => " + grade);
        
    }
    
}
